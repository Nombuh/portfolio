## Material UI Portfolio

A portfolio single page application using React and Material UI

### Portfolio is available here [Material-UI-Portfolio Demo](https://materialui-portfolio.netlify.app/)

#### Installing

Clone the repository using following command or download

```
git clone https://gitlab.com/Nombuh/portfolio.git
```

#### To install dependency

```
npm install
```

#### To start the server

```
npm start
```

#### For Production Build

```
npm run build
```

Server will be available at http://127.0.0.1:3000 in your browser

## Author

<blockquote>
Nombuso Khuzwayo
Email: sthelonombusor@gmail.com
</blockquote>

========Thank You !!!=========
