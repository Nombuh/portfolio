import React,{Fragment} from "react";
import "./portfolio.scss";

const Portfolio = () => { 
  return (
   <Fragment >
       <div className="card-group" >
       <div className="card">
       <h1>React & Tailwind css</h1>
       <p>Single Page Application developed using React and Tailwind css</p>
       <button className="button-group"onClick={()=> window.open('https://gitlab.com/Nombuh/landingpage')}>View On Gitlab</button>
      
      </div>
      <div className="card">
     <h1>Cypress Tests and API testing using Postman</h1>
     <p> Cypress automation tests for a public e-commerce website using page object model and mocha awesome for downloading reports.</p>
     <button className="button-group" onClick={()=> window.open('https://gitlab.com/Nombuh/saucedemo')}>View On Gitlab</button>
   </div>
    <div className="card">
     <h1>.Net Core ,React & Postgres Fullstack</h1>
     <p>Desk reservoir App written in react , ant design and .net core backend ,using flyway migration tools and docker ,this application allows employees who works in a hybrid environment book a desk in the office.
       </p>
       <button className="button-group" onClick={()=> window.open('https://gitlab.com/deskreservoir/reservedeskapp')}>View On Gitlab</button>
   </div>
   <div className="card">
     <h1>Selenium Automation Tests</h1>
     <p>Selenium automation tests for a public website using page object model and Java.</p>
     <button className="button-group" onClick={()=>window.open('https://gitlab.com/selenium-java-tests')}>View On Gitlab</button>
   </div>
 
     </div>
    
    

   </Fragment>
  );
};

export default Portfolio;

