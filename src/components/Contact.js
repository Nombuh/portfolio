import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

import Send from "@material-ui/icons/Send";
import {useForm,ValidationError} from '@formspree/react';

const useStyles = makeStyles((theme) => ({
  contactContainer: {
    background: "#071E26",
    height: "100vh",
  },
  heading: {
    color: "#96be25",
    textAlign: "center",
    textTransform: "uppercase",
    marginBottom: "1rem",
  },
  form: {
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    position: "absolute",
  },
  input: {
    color: "#fff",
  },
  button: {
    marginTop: "1rem",
    color: "#96be25",
    borderColor: "#96be25",
  },
  field: {
    margin: "1rem 0rem",
  },
}));

const InputField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "#d3eaf2",
    },
    "& label": {
      color: "#d3eaf2",
      
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#96be25",
      },
      "&:hover fieldset": {
        borderColor: "",
      },
      "&.Mui-focused fieldset": {
        color: "#fff",
        borderColor: "",
      },
    },
  },
})(TextField);

const Contact = () => {
  const [state,handleSubmit] = useForm("mwkyknqg");

  const classes = useStyles();

  if(state.succeeded){
    return <h1>Email sent successfully!</h1>
  }
  return (
    <Box component="div" className={classes.contactContainer}>
      <Grid container justify="center">
        <Box component="form" className={classes.form}>
          <Typography variant="h5" className={classes.heading}>
            Hire or Contact me...
          </Typography>
          <form onSubmit={handleSubmit}>
          <InputField
            id="name"
            name="name"
            type="text"
            fullWidth={true}
            label="Name"
            variant="outlined"
            inputProps={{ className: classes.input }}
          />
          <ValidationError
          prefix="Name"
          field="name"
          errors={state.errors}/>
          <InputField
            id="email"
            type="email"
            name="_replyto"
            // name="email"
            fullWidth={true}
            label="Email"
            variant="outlined"
            inputProps={{ className: classes.input }}
            className={classes.field}
          />
           <ValidationError
          prefix="Email"
          field="email"
          errors={state.errors}/>
          <InputField
            id="message"
            name="message"
            fullWidth={true}
            label="Message"
            variant="outlined"
            multiline
            rows={4}
            inputProps={{ className: classes.input }}
          />
           <ValidationError
          prefix="Message"
          field="message"
          errors={state.errors}/>
          <Button
            type="submit"
            value="send"
            disabled={state.submitting}
            variant="outlined"
            fullWidth={true}
            endIcon={<Send />}
            className={classes.button}
          >
            Contact Me
          </Button>
          </form>
        </Box>
      </Grid>
    </Box>
  );
};

export default Contact;
