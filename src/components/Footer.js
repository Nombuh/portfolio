import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import GitHubIcon from '@material-ui/icons/GitHub';

const useStyles = makeStyles({
  bottomNavContainer: {
    background: "#134B5F",
  },
  root: {
    "& .MuiSvgIcon-root": {
      fill: "#ffffff",
      "&:hover": {
        fill: "#96be25",
        fontSize: "1.8rem",
      },
    },
  },
});

const Footer = () => {
  const classes = useStyles();

  return (
    <BottomNavigation className={classes.bottomNavContainer}>
      <BottomNavigationAction icon={<LinkedInIcon />} className={classes.root} onClick={()=>window.open('https://www.linkedin.com/in/nombuso-khuzwayo-b72a77122/')}/>
      <BottomNavigationAction icon={<GitHubIcon />} className={classes.root} onClick={()=>window.open('https://gitlab.com/dashboard/projects')}/>
    </BottomNavigation>
  );
};
export default Footer;
